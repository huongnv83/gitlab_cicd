package com.telsoft.gitlab_cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabCicdApplication.class, args);
    }

}
